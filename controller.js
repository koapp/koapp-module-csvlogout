(function() {
    'use strict';
  
    angular
      .module('csvlogout', [])
      .controller('csvlogoutController', loadFunction);
  
    loadFunction.$inject = ['$scope', 'structureService', '$location', '$http', '$rootScope', '$window'];
  
    function loadFunction($scope, structureService, $location, $http, $rootScope, $window) {
      // Register upper level modules
      structureService.registerModule($location, $scope, 'csvlogout');
      // --- Start csvlogoutController content ---
      
      localStorage.setItem("csvLoginData", "logedOut");
      
      var tempUrl = angular.copy($rootScope.backUrl);
      console.log($rootScope.backUrl);
      if (tempUrl.length - 2 > -1) {
        $rootScope.backUrl.pop();
        $location.path(tempUrl[tempUrl.length - 2]);
      }
      
      // --- End csvlogoutController content ---
    }
  }());
  